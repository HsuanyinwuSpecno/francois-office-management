import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Office } from '../shared/shared-models';
import { Observable } from 'rxjs';
import { concatMap, map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public offices: Observable<Office[]>
  constructor(
    private db: AngularFirestore
  ) { }

  ngOnInit(): void {
    this.offices = this.db.collection('officeList').snapshotChanges().pipe(
      map(docData=> {
        return docData.map(doc => {
          return {
            id: doc.payload.doc.id,
            color: doc.payload.doc.data()['color'],
            email: doc.payload.doc.data()['email'],
            location: doc.payload.doc.data()['location'],
            maxOccupants: doc.payload.doc.data()['maxOccupants'],
            name: doc.payload.doc.data()['name'],
            tellNumber: doc.payload.doc.data()['name'],
          }
        })
      }
    )
    )
  }

}
