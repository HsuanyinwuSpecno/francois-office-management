export interface Office {
  id: string,
  name: string,
  location: string,
  email: string,
  tellNumber?: string,
  maxOccupants: number,
  color?: string;
}

export interface Staff {
  id: string,
  officeId: string,
  fullName: string,
}