import { NgModule } from "@angular/core";
import { Route, RouterModule } from "@angular/router";
import { OfficeViewComponent } from "./office-view.component";

const routes: Route[] = [
  {
    path: ':officeId',
    component: OfficeViewComponent,
  },
];

@NgModule({
  declarations: [
    OfficeViewComponent
  ],
  imports: [
    RouterModule.forChild(routes),

  ]
})
export class OfficeViewModule { }