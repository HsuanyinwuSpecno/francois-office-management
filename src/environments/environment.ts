// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDvgYrsuE8vFunaaeKQp9kprWLuJZWcjy8",
    authDomain: "ng-office-ea8b8.firebaseapp.com",
    projectId: "ng-office-ea8b8",
    storageBucket: "ng-office-ea8b8.appspot.com",
    messagingSenderId: "792600988052",
    appId: "1:792600988052:web:fbdeb91e100f02b53ba781",
    measurementId: "G-GD4E521S1K"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
